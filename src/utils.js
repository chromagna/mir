export const EMPTY_OBJECT = {};
export const EMPTY_ARRAY = [];
export const DEFAULT = 0;
export const RECYCLED = 1;
export const TEXT = 1;
export const XLINK_NS = "http://www.w3.org/1999/xlink";
export const SVG_NS = "http://www.w3.org/2000/svg";
export const undef = undefined;
export const map = EMPTY_ARRAY.map;
export const eventProxy = (event) => event.currentTarget.events[event.type](event);
export const merge = (a, b) => Object.assign({}, a, b);
export const isFunction = (fn) => typeof fn === "function";
export const isArray = Array.isArray;
export const invokeOrReturn = (arg, fn) => (isFunction(fn) ? fn(arg) : fn);
export const depsChanged = (argsOld, argsNew) => {
	return !argsOld || argsOld.length !== argsNew.length || argsNew.some((arg, index) => arg !== argsOld[index]);
};
export const safeHasProperty = (obj, prop) => (obj ? Object.prototype.hasOwnProperty.call(obj, prop) : false);
export const safelyParseJson = (str) => {
	try {
		return JSON.parse(str);
	} catch (e) {
		return null;
	}
};

/**
https://github.com/JedWatson/classnames
The MIT License (MIT)

Copyright (c) 2018 Jed Watson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
export function classNames() {
	let classes = [];
	for (let i = 0; i < arguments.length; i++) {
		let arg = arguments[i];
		if (!arg) continue;
		let argType = typeof arg;
		if (argType === "string" || argType === "number") {
			classes.push(arg);
		} else if (Array.isArray(arg)) {
			if (arg.length) {
				let inner = classNames.apply(null, arg);
				if (inner) {
					classes.push(inner);
				}
			}
		} else if (argType === "object") {
			if (arg.toString === Object.prototype.toString) {
				for (let key in arg) {
					if (Object.hasOwnProperty.call(arg, key) && arg[key]) {
						classes.push(key);
					}
				}
			} else {
				classes.push(arg.toString());
			}
		}
	}
	return classes.join(" ");
}
