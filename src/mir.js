import { setup } from "./css";
import { h } from "./h";

const customPrefixer = (key, value) => `${key}:${value};`;
setup(h, customPrefixer, "");

export { css, glob, keyframes, styled, withTheme } from "./css";
export { globalStore } from "./globalstore";
export { cloneElement } from "./h";
export { createContext, removeAllHookStates, useCallback, useContext, useEffect, useImperativeHandle, useLayoutEffect, useMemo, useReducer, useRef, useState } from "./hooks";
export { defineStore, useStore } from "./instancedstore";
export { Portal } from "./portal";
export { enqueueRender, Fragment, render, update } from "./render";
export { goto, Link, Route, routeParams, routeQuery } from "./router";
export { classNames } from "./utils";
export { h };
