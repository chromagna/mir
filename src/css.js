// This is Goober.
// https://github.com/cristianbote/goober
/* eslint-disable no-empty */
const objParser = /(?:([a-z0-9-%@]+) *:? *([^{;]+?);|([^;}{]*?) *{)|(})/gi;
const whitespaceRemover = /\/\*[\s\S]*?\*\/|\s{2,}|\n/gm;
let cache = {};
let GID = "_goober";
let ssr = { data: "" };

const astish = (val) => {
	let tree = [{}];
	let block;

	while ((block = objParser.exec(val.replace(whitespaceRemover, "")))) {
		if (block[4]) tree.shift();
		if (block[3]) tree.unshift((tree[0][block[3]] = tree[0][block[3]] || {}));
		else if (!block[4]) tree[0][block[1]] = block[2];
	}

	return tree[0];
};

const compile = (str, defs, data) => {
	return str.reduce((out, next, i) => {
		let tail = defs[i];
		if (tail && tail.call) {
			let res = tail(data);
			let className = res && res.props && res.props.className;
			let end = className || (/^go/.test(res) && res);
			if (end) tail = `.${end}`;
			else if (res && typeof res == "object") tail = res.props ? "" : parse(res, "");
			else tail = res;
		}
		return out + next + (tail == null ? "" : tail);
	}, "");
};

let getSheet = (target) => {
	try {
		let sheet = target ? target.querySelector(`#${GID}`) : self[GID];
		if (!sheet) {
			sheet = (target || document.head).appendChild(document.createElement("style"));
			sheet.innerHTML = " ";
			sheet.id = GID;
		}
		return sheet.firstChild;
	} catch (e) {}
	return target || ssr;
};

let stringify = (data) => {
	let out = "";
	for (let p in data) {
		let val = data[p];
		out += p + (typeof val == "object" ? stringify(data[p]) : data[p]);
	}
	return out;
};

let parse = (obj, selector) => {
	let outer = "";
	let blocks = "";
	let current = "";
	let next;
	for (let key in obj) {
		let val = obj[key];
		if (typeof val == "object") {
			next = selector
				? selector.replace(/([^,])+/g, (sel) => {
						return key.replace(/([^,])+/g, (k) => {
							if (/&/g.test(k)) return k.replace(/&/g, sel);
							return sel ? sel + " " + k : k;
						});
				  })
				: key;
			if (key[0] == "@") {
				if (key[1] == "f") blocks += parse(val, key);
				else blocks += key + `{${parse(val, key[1] == "k" ? "" : selector)}}`;
			} else blocks += parse(val, next);
		} else {
			if (key[0] == "@" && key[1] == "i") outer = `${key} ${val};`;
			else {
				current += parse.p ? parse.p(key.replace(/[A-Z]/g, "-$&").toLowerCase(), val) : key.replace(/[A-Z]/g, "-$&").toLowerCase() + `:${val};`;
			}
		}
	}
	if (current[0]) {
		next = selector ? `${selector}{${current}}` : current;
		return outer + next + blocks;
	}
	return outer + blocks;
};

export const extractCss = (target) => {
	let sheet = getSheet(target);
	let out = sheet.data;
	sheet.data = "";
	return out;
};

let update = (css, sheet, append) => {
	sheet.data.indexOf(css) < 0 && (sheet.data = append ? css + sheet.data : sheet.data + css);
};

let toHash = (str) => `go${str.split("").reduce((out, i) => (101 * out + i.charCodeAt(0)) >>> 0, 11)}`;

let hash = (compiled, sheet, global, append, keyframes) => {
	let stringifiedCompiled = typeof compiled == "object" ? stringify(compiled) : compiled;
	let className = cache[stringifiedCompiled] || (cache[stringifiedCompiled] = toHash(stringifiedCompiled));
	if (!cache[className]) {
		let ast = typeof compiled == "object" ? compiled : astish(compiled);
		cache[className] = parse(keyframes ? { ["@keyframes " + className]: ast } : ast, global ? "" : "." + className);
	}
	update(cache[className], sheet, append);
	return className;
};

export function css(val) {
	let ctx = this || {};
	let _val = val.call ? val(ctx.p) : val;
	return hash(_val.map ? compile(_val, [].slice.call(arguments, 1), ctx.p) : _val, getSheet(ctx.target), ctx.g, ctx.o, ctx.k);
}

export let glob = css.bind({ g: 1 });
export let keyframes = css.bind({ k: 1 });
let h, useTheme;

export function setup(pragma, prefix, theme) {
	parse.p = prefix;
	h = pragma;
	useTheme = theme;
}

export function withTheme(theme) {
	useTheme = theme;
}

const PADDING = {
	padding: { property: ["padding"] },
	p: { property: ["padding"] },
	pt: { property: ["paddingTop"] },
	pr: { property: ["paddingRight"] },
	pb: { property: ["paddingBottom"] },
	pl: { property: ["paddingLeft"] },
	pl: { property: ["paddingLeft"] },
	px: { property: ["paddingLeft", "paddingRight"] },
	py: { property: ["paddingTop", "paddingBottom"] },
};

const MARGIN = {
	margin: { property: ["margin"] },
	m: { property: ["margin"] },
	mt: { property: ["marginTop"] },
	mr: { property: ["marginRight"] },
	mb: { property: ["marginBottom"] },
	ml: { property: ["marginLeft"] },
	mx: { property: ["marginLeft", "marginRight"] },
	my: { property: ["marginTop", "marginBottom"] },
};

const COLOR = {
	bg: { property: ["backgroundColor"] },
};

const PADDING_PROPS = [...Object.keys(PADDING)];
const MARGIN_PROPS = [...Object.keys(MARGIN)];
const COLOR_PROPS = [...Object.keys(COLOR)];
const CONVERTABLE = [...Object.keys(PADDING), ...Object.keys(MARGIN), ...Object.keys(COLOR)];

const applySpaceScale = (scale, props, propName, meta) => {
	let v = props[propName];
	let n = 0;

	if (typeof v === "number") {
		if (v >= 0 && v < scale.length) {
			// use provided scale
			n = `${scale[v]}px`;
		} else {
			// out of bounds of scale, use % if 0..1
			if (v > 0 && v < 1) {
				n = v * 100 + "%";
			} else {
				// fallback to px
				n = `${v}px`;
			}
		}
	} else if (typeof v === "string") {
		n = v;
	}

	delete props[propName];
	for (const a of meta.property) {
		props[a] = n;
	}
};

const applyScale = (scale, props, propName, meta) => {
	let v = props[propName]; // props.bg = primary.base
	let parsed = v;
	let r = v.split(".").reduce((o, i) => o[i], scale);
	if (r) parsed = r;
	for (const a of meta.property) {
		props[a] = parsed;
	}
};

const convertSpace = (props) => {
	// If no convertable props, immediately return
	if (!CONVERTABLE.some((pp) => Object.hasOwnProperty.call(props, pp))) {
		return;
	}

	PADDING_PROPS.map((pp) => Object.hasOwnProperty.call(props, pp) && applySpaceScale(props.theme?.space || [], props, pp, PADDING[pp]));
	MARGIN_PROPS.map((pp) => Object.hasOwnProperty.call(props, pp) && applySpaceScale(props.theme?.space || [], props, pp, MARGIN[pp]));
	// COLOR_PROPS.map(pp => Object.hasOwnProperty.call(props, pp) && applyScale(props.theme?.colors || {}, props, pp, COLOR[pp]));
};

export function styled(tag, forwardRef) {
	let _ctx = this || {};
	return function wrapper() {
		let _args = arguments;
		function Styled(props, ref) {
			let _props = Object.assign({}, props);
			let _previousClassName = _props.className || Styled.className;
			_ctx.p = Object.assign({ theme: useTheme && useTheme() }, _props);
			useTheme && convertSpace(_ctx.p);
			_ctx.o = /\s*go[0-9]+/g.test(_previousClassName);
			_props.className = css.apply(_ctx, _args) + (_previousClassName ? " " + _previousClassName : "");
			if (forwardRef) _props.ref = ref;
			return h(_props.as || tag, _props);
		}
		return forwardRef ? forwardRef(Styled) : Styled;
	};
}
