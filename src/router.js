import { pathToRegexp } from "path-to-regexp";
import { useMemo, useRef } from "./hooks";
import { enqueueRender, h, useEffect, useState } from "./mir";

let handlers = [];
export let routeParams = {};
export let routeQuery = {};

// Drained by postRender after the first render following a route change.
export let preRenderRouterHandlers = [];
export let postRenderRouterHandlers = [];

// Always-filled handler stores
let _preRenderRouterHandlers = [];
let _postRenderRouterHandlers = [];
export const registerPreRenderRouterHandler = (handler) => _preRenderRouterHandlers.push(handler);
export const registerPostRenderRouterHandler = (handler) => _postRenderRouterHandlers.push(handler);

// fireHandlers will call fillPostRenderQueue on route change, which fills postRenderRouterHandlers
// postRender will consume these handlers and drain the queue in the order it was filled
const fillPostRenderQueue = () => postRenderRouterHandlers.push(..._postRenderRouterHandlers);
const fillPreRenderQueue = () => preRenderRouterHandlers.push(..._preRenderRouterHandlers);
handlers.push(fillPreRenderQueue);
handlers.push(fillPostRenderQueue);

const fireHandlers = () => handlers.forEach((handler) => handler());

export const goto = (to, body = {}) => {
	history.pushState(body, "", to);
	fireHandlers();
};

export const Link = ({ to, body, children, ...rest }) => {
	const onLinkClick = (e) => {
		e.preventDefault();
		goto(to, body || {});
	};

	return (
		<a href={to} onclick={(e) => onLinkClick(e)} {...rest}>
			{children}
		</a>
	);
};

export const Route = (props) => {
	const cfg = useMemo(
		() => getRouteExpression(props.path, props.strict ?? false, props.end ?? true, props.sensitive ?? false),
		[props.path, props.strict, props.end, props.sensitive]
	);
	let [config, _] = useState(cfg);
	const match = useRef({});
	const handlersSet = useRef(false);
  const handler = () => {
    match.current = getCurrentRouteMatch(config);
    enqueueRender();
  };

	if (!handlersSet.current) {
		window.addEventListener("load", handler);
		window.addEventListener("popstate", handler);
		handlers.push(handler);
		handlersSet.current = true;
		handler();
	}

	useEffect(() => {
		return () => {
			window.removeEventListener("load", handler);
			window.removeEventListener("popstate", handler);
			handlers.splice(handlers.indexOf(handler), 1);
		};
	}, []);

	if (match.current && props.component) return <props.component match={match.current} {...props} />;
  else if (match.current && props.render) return <div>{props.render({ match: match.current, config })}</div>;
	else if (match.current) return props.children;
	else return null;
};

const getRouteExpression = (path, strict, end, sensitive) => {
	let result = { re: null, params: [] };

	if (path === "*") result.re = /[\w\W]*/i;
	else result.re = pathToRegexp(path, result.params, { end, strict, sensitive });

	return result;
};

const getCurrentRouteMatch = (config) => {
	const url = `${location.pathname}${location.search}`;
	let queryString = "";
	let index = url.indexOf("?");
	if (index > -1) {
		queryString = url.substr(index);
		url = url.slice(0, index);
	}
	let result = config.re.exec(url);
	let match = {};
	if (result) {
		match.params = Object.assign(routeParams, getParams(config.params, result));
		match.query = Object.assign(routeQuery, parseQuery(queryString));
		routeParams = match.params;
		routeQuery = match.query;
		return match;
	}
	return false;
};

const getParams = (keys = [], matched) => {
	let params = {};
	for (let i = 0; i < keys.length; i++) {
		params[keys[i].name] = matched[i + 1];
	}
	return params;
};

const parseQuery = (queryString) => {
	let query = {};

	queryString = queryString.trim().replace(/^(\?|#||&)/, "");

	if (!queryString) return null;

	queryString.split("&").forEach((param) => {
		const [rawKey, rawVal] = param.split("=");
		const [key, val] = [decodeURIComponent(rawKey), rawVal ? decodeURIComponent(rawVal) : null];
		query[key] = val;
	});

	return query;
};
