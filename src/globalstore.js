let eventHandlers = [];

const off = (h) => () => eventHandlers.splice(eventHandlers.indexOf(h) >>> 0, 1);

const _globalStore = () => {
	let _state = {};
	let _actions = {};
	let _root = null;
	let _instances = [];

	const _update = (action, state, instance = false) => {
		if (action) {
			if (!instance) {
				_state = { ..._state, ...state };
				eventHandlers.map((handler) => handler(_state, action));
			}

			_instances.map((inst) => {
				inst.forceUpdates && inst.update(action, {}, true);
			});
		}
	};

	return {
		get instances() {
			return _instances;
		},

		get events() {
			return eventHandlers;
		},

		get state() {
			return _state;
		},
		set state(v) {
			_state = v;
		},

		get root() {
			return _root;
		},
		set root(v) {
			_root = v;
		},

		get actions() {
			return _actions;
		},
		set actions(v) {
			_actions = v;
		},

		on: (h) => {
			eventHandlers.push(h);
			return off(h);
		},

		update: (action, state, instance) => _update(action, state, instance),

		dispatch: (action, ...payload) => {
			let result;

			if (typeof action === "string" && _actions[action]) {
				result = _actions[action](_state, ...payload);
			} else {
				result = action(_state, ...payload);
			}

			if (result) {
				if (result.then) return result.then((res) => _update(action, res));
				return _update(action, result);
			}
		},

		register: (instance) => {
			_instances.some((inst) => inst.id === instance.id) || _instances.push(instance);
		},
	};
};

const globalStore = _globalStore();

export { globalStore };
