import { createContext, useContext } from "../src/hooks";
import { css, glob, h, Link, Route, render, styled, useState } from "../src/mir";

glob`body{font-family:serif;}a,a:visited{color:blue;}a:hover{color:navy;}a:active{color:purple;}`;

const NavItem = styled("li")`
	margin-right: 5px;
`;

const App = () => {
	const [count, setCount] = useState(0);

	return (
		<div class={css`padding: 0px;`}>
			<h1>mir</h1>
			<div>
        <ul class={css`display:flex;list-style-type:none;margin:0;padding:0;`}>
					<NavItem>
						<Link to="/">hello</Link>
					</NavItem>
					<NavItem>
						<Link to="/about">about</Link>
					</NavItem>
				</ul>
			</div>
			<div class={css`height: 200px;`}>
				<Route path="/">
					<p>Thanks for giving mir a chance.</p>
					<h1>ヽ(´▽`)/</h1>
					<button onClick={() => setCount(count + 5)}>{count} ++</button>
				</Route>
				<Route path="/about">
					<p>mir is a small, fast and familiar frontend framework.</p>
					<p>It is being built while maintaining these philosophies:</p>
					<ul>
						<li>It should be fast and efficient: render only when necessary and with smart DOM diffing.</li>
						<li>Routing should be simple, powerful and included in the base bundle.</li>
						<li>It should provide an easy way to share state between distant components regardless of their position in the node tree.</li>
						<li>useEffect, useState, useRef, useCallback, useMemo and useReducer should all work exactly how you expect them to.</li>
					</ul>
					<p>TLDR; If you're familiar with using React hooks and functional components, then you're familiar with mir.</p>
					<p>
						<a href="#">https://mir.dev/docs</a>
					</p>
				</Route>
			</div>
		</div>
	);
};

render(document.querySelector("#root"), App);
