import { h } from "./h";
import { useEffect, useRef } from "./hooks";
import { patch } from "./patch";
import { SVG_NS } from "./utils";

const getEl = (id) => document.getElementById(id);
export const getId = () => Math.random().toString(36).substring(2, 12);

const Proxy = (props) => props.children[0];

const createEl = (tagName) => {
	let el;

	if (tagName === "svg" || tagName === "path") {
		el = document.createElementNS(SVG_NS, tagName);
		if (tagName === "svg") el.setAttribute("xmlns", SVG_NS);
	} else {
		el = document.createElement(tagName);
	}

	return el;
};

const setAttrs = (el, attrs) => {
	if (!el) return;
	Object.keys(attrs).forEach((attr) => {
		el.setAttribute(attr, attrs[attr]);
	});
};

export const Portal = (props) => {
	const id = useRef(null);
	const to = useRef(props.to ?? null);
	const open = props.open ?? true;
	const patchNode = useRef(null);
	const tag = useRef(props.tag ?? "div");
	const attrs = props.attrs ?? {};
	const ref = useRef(props.ref);

	// Generate an ID if we haven't already.
	if (!id.current) id.current = getId();

	// Watch attributes and reapply them as needed
	useEffect(() => {
		setAttrs(getEl(id.current), attrs);
	}, [attrs]);

	if (props.ref && !props.ref.current) props.ref.current = getEl(id.current);

	useEffect(() => {
		if (!open) {
			getEl(id.current)?.parentNode.removeChild(getEl(id.current));
			id.current = null;
			patchNode.current = null;
		}
		if (open) {
			if (!getEl(id.current)) {
				let div;
				if (to.current) {
					div = createEl(tag.current);
					div.setAttribute("id", id.current);
					setAttrs(div, attrs);
					getEl(to.current).appendChild(div);
				}

				if (!to.current) {
					div = createEl(tag.current);
					div.setAttribute("id", id.current);
					setAttrs(div, attrs);
					document.body.appendChild(div);
				}

				patchNode.current = patch(patchNode.current, h(Proxy, props), div);
			}
		}
	}, [open]);

	// A portal should be open, but the target container (body child) hasn't been created yet.
	if (id.current && open && !to.current && !getEl(id.current)) {
		let div = createEl(tag.current);
		div.setAttribute("id", id.current);
		setAttrs(div, attrs);
		document.body.appendChild(div);
	}

	// A portal should be open, but the target container (to.current child) hasn't been created yet.
	if (id.current && open && to.current && !getEl(id.current)) {
		let div = createEl(tag.current);
		div.setAttribute("id", id.current);
		setAttrs(div, attrs);
		getEl(to.current).appendChild(div);
	}

	// A portal should be open and the target container exists. Patch it.
	if (id.current && open) {
		if (getEl(id.current)) {
			let div = getEl(id.current);
			patchNode.current = patch(patchNode.current, h(Proxy, props), div);
		}
	}

	// If the 'open' prop isn't provided, it will default to true.
	// This branch only check for an existing, false 'open' prop.
	if (!open) {
		if (id.current && getEl(id.current)) {
			getEl(id.current)?.parentNode.removeChild(getEl(id.current));
			id.current = null;
			patchNode.current = null;
		}
	}

	useEffect(() => {
		return () => {
			getEl(id.current)?.parentNode.removeChild(getEl(id.current));
			id.current = null;
			patchNode.current = null;
		};
	}, []);

	return null;
};
