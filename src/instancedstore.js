import { globalStore } from "./globalstore";
import { safelyParseJson } from "./utils";

const instancedStore = (id) => {
	let _state = {};
	let _actions = {};
	let _root = null;
	let _events = [];
	let _id = null;
	let _forceUpdates = true;
	let _persist = true;

	_id = id;

	const _update = (action, state, prevent = false) => {
		if (!_forceUpdates) {
			if (_persist) {
				_state = { ..._state, ...state };
				eventMap.get(_id)?.forEach((handler) => handler(_state));
			}
			return;
		}
		if (prevent) {
			globalStore.events.map((handler) => handler(globalStore.state, action));
		} else {
			_state = { ..._state, ...state };
			eventMap.get(_id)?.forEach((handler) => handler(_state));
		}
	};

	const off = (handler) => _events.splice(_events.indexOf(handler) >>> 0);

	return {
		get id() {
			return _id;
		},

		get persist() {
			return _persist;
		},
		set persist(v) {
			_persist = v;
		},

		get state() {
			return _state;
		},
		set state(v) {
			_state = v;
		},

		get root() {
			return _root;
		},
		set root(v) {
			_root = v;
		},

		get actions() {
			return _actions;
		},
		set actions(v) {
			_actions = v;
		},

		get forceUpdates() {
			return _forceUpdates;
		},
		set forceUpdates(v) {
			_forceUpdates = v;
		},

		on: (h) => {
			if (!h || !_id) return;
			let currentEvents = eventMap.get(_id) || [];
			currentEvents.push(h);
			eventMap.set(_id, currentEvents);
			return off(h);
		},

		update: (action, state, prevent) => _update(action, state, prevent),

		dispatch: (action, ...payload) => {
			let result;

			if (typeof action === "string" && _actions[action]) {
				result = _actions[action](_state, ...payload);
			} else {
				result = action(_state, ...payload);
			}

			if (result) {
				return result.then ? result.then((res) => _update(action, res)) : _update(action, result);
			}
		},
	};
};

let storeMap = new Map();
let eventMap = new Map();

let getDefaultStorageOptions = (opts) => ({
	persist: opts.persist ?? true,
	forceUpdates: opts.forceUpdates ?? true,
});

export const useStore = (store) => {
	return [store.state, store.dispatch];
};

export const defineStore = (id, initialState = {}, opts = {}) => {
	id = `${id}`;

	let options = getDefaultStorageOptions(opts);
	let store = storeMap.get(id);
	let currentEvents = eventMap.get(id) || [];

	if (store) {
		store.forceUpdates = options.forceUpdates;
	} else {
		store = instancedStore(id);
		if (options.persist) {
			store.state = safelyParseJson(localStorage.getItem(id)) || initialState;
			currentEvents.push((state) => localStorage.setItem(id, JSON.stringify(state)));
		} else {
			store.state = initialState;
		}
	}

	store.persist = options.persist;
	store.forceUpdates = options.forceUpdates;
	store.root = globalStore.root;
	globalStore.register(store);
	options.forceUpdates && currentEvents.push((state) => globalStore.update("...", state, true));

	storeMap.set(id, store);
	eventMap.set(id, currentEvents);

	return store;
};
