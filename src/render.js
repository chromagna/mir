import { globalStore } from "./globalstore";
import { h } from "./h";
import {
	checkIns,
	clearCheckins,
	clearEffects,
	clearUniqueMap,
	componentsWithState,
	hookIndexToCleanup,
	pendingCleanups,
	pendingLayoutCleanups,
	postEffects,
	removeHookStateForComponent,
	resetHookState,
} from "./hooks";
import { createVNode, patch } from "./patch";
import { postRenderRouterHandlers, preRenderRouterHandlers } from "./router";
import { DEFAULT, EMPTY_OBJECT } from "./utils";

export const Fragment = (props) => props.children || [];

let renderQueue = [];
export const enqueueRender = () => renderQueue.length < 2 && renderQueue.push(1);
let shouldRender = () => renderQueue.length;

export const render = (root, view) => {
	const node = createVNode(Fragment, EMPTY_OBJECT, [], view, EMPTY_OBJECT, DEFAULT);
	globalStore.root = root;
	sub(node);
};

function onlyUnique(value, index, self) {
	return self.indexOf(value) === index;
}

const makeUniqueArray = (arr) => arr?.filter(onlyUnique) || [];

const PRE = 0;
const POST = 1;

const preUpdate = () => {
	handleCleanups(PRE);

	for (; preRenderRouterHandlers.length; preRenderRouterHandlers.shift()());

	clearCheckins();
	clearUniqueMap();
};

const postUpdate = () => {
	let hadEffects = postEffects.length;
	for (let i = 0; i < postEffects.length; i++) postEffects?.[i]?.[0]?.(postEffects[i][1]);
	clearEffects();

	hadEffects && handleCleanups(POST);

	for (; postRenderRouterHandlers.length; postRenderRouterHandlers.shift()());
};

const handleCleanups = (preOrPost) => {
	for (const [component, componentKeys] of Object.entries(componentsWithState)) {
		checkIns[component] = makeUniqueArray(checkIns[component]);
		let checkInKeys = checkIns[component];
		const cleanups = preOrPost === PRE ? pendingLayoutCleanups : pendingCleanups;

		// There may be a more performance-friendly way to discover if checkInKeys includes any componentKey
		for (const componentKey of componentKeys) {
			if (!checkInKeys || !checkInKeys.includes(componentKey)) {
				let hookIndexes = cleanups[component]?.[componentKey];
				if (hookIndexes) {
					for (const hookIndex of hookIndexes) {
						hookIndexToCleanup[component][componentKey][hookIndex]?.func?.();
						// `remounted` tells the useEffect that it doesn't need to re-run a cleanup next evaluation cycle.
						hookIndexToCleanup[component][componentKey][hookIndex].hook.remounted = true;
					}
					delete hookIndexToCleanup[component][componentKey];
				}
				cleanups[component]?.[componentKey] && delete cleanups[component][componentKey];
				// TODO:
				// Whether or not this is PRE or POST, this should run, but only after all effects have been ran.
				// Maybe this should be postponed until after the render cycle?
				POST && removeHookStateForComponent(component, componentKey);
			}
		}
	}
};

export let update;

const sub = (view) => {
	let node;

	update = () => {
		resetHookState(view);
		preUpdate();
		node = patch(node, h(view.element, {}), globalStore.root);
		postUpdate();
		renderQueue.pop();
	};

	globalStore.on(update);
	enqueueRender();
	raf();
};

const raf = () => {
	if (shouldRender()) update();
	requestAnimationFrame(raf);
};
