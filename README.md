# mir

mir is a small (8.2kB gzipped umd bundle) frontend framework with an API identical to that of React with hooks. The hooks API
is elegant and functional. This project is an attempt to recreate the hooks API with a minimal footprint while introducing
some nice-to-have utilities here and there.

[toc]

# Quickstart

If you know how to use React with hooks, you probably don't need this quickstart introduction.

## Basic composition

```javascript
import { useState } from "mir";

const Component = () => {
    const [count, setCount] = useState(0);
    
    return (
        <div>
            <button onClick={() => setCount(count + 1)}>
                {count} ++
            </button>
        </div>
    );
};
```

## Rendering to a target

```javascript
import { render } from "mir";

render(document.querySelector("#root"), Component);
```

# CSS

Goober is used as mir's built-in CSS solution. Goober is amazing, and less than 1kb.

https://github.com/cristianbote/goober

```javascript
import { styled } from "mir"; // This is goober's `styled` export.
import { css } from "mir"; // This is goober's `css` export.
import { glob } from "mir"; // This is goober's `glob` export.
import { keyframes } from "mir"; // This is goober's `keyframes` export.
import { extractCss } from "mir"; // This is goober's `extractCss` export.
```

The API is unchanged. See Goober docs for API reference.

## styled

Similar to `styled-components`, the `styled` export can be made aware
of theme context.

```javascript
import { createContext, useContext, withTheme } from "mir";

const themes = {
    dark: {
        background: #111,
        color: #888,
    },
    light: {
        background: #f4f5f7,
        color: #111,
    },
};

// Create context
const ThemeContext = createContext(themes.dark);
// Define a function that returns that context
const themeContextGetter = () => useContext(ThemeContext);
// Provide that function to `withTheme`
withTheme(themeContextGetter);
```

`styled` will now pass the context returned by `themeContextGetter`
as props to components created with it.

```javascript
import { styled } from "mir";

const Card = styled(Box)((p) => ({
    background: p.background,
    color: p.color,
}));
```

Paired with `styled-system`, this theming pattern is quite nice to work with.

# Portals

Portals are a way to render components outside of the scope of the root render target.
When opened, they will essentially create a new render target that gets diffed whenever
the component consuming the portal is evaluated.

Import the `Portal` component:

```javascript
import { Portal } from "mir";
```

## Key points

There are some things to keep in mind when using portals.

### Controlling the open/close state

Pass a boolean to the `open` prop of a portal to control its opened state.

### Use the 'key' property

Portals **require** a key property or they may render unpredictably (out of order, with the wrong contents, etc).

```javascript
// Creates a div element as a child of document.body.
<Portal>
    <div/>
</Portal>
```

This requirement can be cumbersome if you need to, say, manage an unknown number of `Notification`-type components.

A pattern like this helps:

```javascript
let modalCounter = 0;

export const useModal = ({ ref, key, ...rest }) => {
    const modalKey = useRef(key);
    if (!modalKey.current) modalKey.current = `modal${++modalCounter}`;
    // ...
    return <Portal key={modalKey.current}> ... </Portal>;
};

// Elsewhere..
const Component = () => {
    const { openModal, closeModal, Modal } = useModal();
    // ...
    return <Modal ... />;
};
```

### You must pass a child to a portal

An empty span will suffice. A simple text node is also fine. A Fragment is not considered a valid child for a Portal, but may be used to wrap other elements as usual.

In a future version of mir, this behavior may change such that you do not need to pass a child element. For now, though, it is a requirement.

## Providing a tag

You can define what type of element the portal will create by using the `tag` prop.

```html
<body>
    <div id="modals"></div>
</body>
```

```javascript
<Portal tag="span" key={modalIndex} to="modals">This is my modal</Portal>
```

```html
<body>
    <div id="modals">
        <span id="9dj19f2f91">This is my modal</span>
    </div>
</body>
```

## Using svg and path

When passing `svg` or `path` as the value to the `tag` prop, the portal will use createElementNS when creating itself.

Given this portal target:

```html
<body>
    <svg xmlns="http://www.w3.org/2000/svg" id="paths" width="4000" height="4000"></svg>
</body>
```

Define a portal like this:

```javascript
// Create a <path> in my <svg> target.
// `attrs` is a special Portal-specific prop that spreads its value onto the created element
<Portal to="paths" key={pathIndex} tag="path" attrs={{ d: "M ...", fill: "none", stroke: "#000", "stroke-width": "4" }}><div/></Portal>
```


...and it will be rendered as:

```html
<body>
    <svg xmlns="http://www.w3.org/2000/svg" id="paths" width="4000" height="4000">
        <path ...> <!-- created with createElementNS -->
            <div/>
        </path>
    </svg>
</body>
```

# Keys, an explanation

If you have an array of 3 `Foo` components and iterate them, call them and ultimately render them to the page, they will receive the keys: `0`, `1` and `2` if no keys were explicitly provided to them.

These keys are used as the function's hook index. It's used to retrieve a hook's state from hook state storage. On the next render, assuming that these items are still rendered in the exact same order, they will be able to reference their state by these keys. That's how hooks like useState and useEffect are able to store state between renders. Render order is important here.

What happens when you can't predict render order? Let's say we remove item 2 and replace it with a completely different `Foo`.
Without providing a `key` prop, its hook index will still be `1`, which was the same hook index as the last (now removed)
`Foo` component. This is bad. This means that the new `Foo` component will receive the hook state of the last `Foo` component resulting in weird, unpredictable state problems.

To prevent this, provide a unique key to identicaly components that get rendered alongside each other.
This will ensure that when a component replaces another, it doesn't inherit any state other than its own.

# Forwarding a ref

There is no `forwardRef()`. Just pass the ref as a prop and receive it in the child component. It will be 
available immediately after the first render of the child component.

## Accessing a child element from a parent:

```javascript
const Child = ({ ref }) => {
    return <div ref={ref}>Child element</div>;
};

const Parent = () => {
    const ref = useRef(null);

    useEffect(() => {
        // ref.current is <div>Child element</div>
    }, []);

    return <Child ref={ref}/>;
};
```

## Accessing a parent element from a child:


```javascript
const Child = ({ ref }) => {
    useEffect(() => {
        // ref.current is <div>Parent element</div>
    }, []);
};

const Parent = () => {
    const ref = useRef(null);

    return (
        <div>
            <div ref={ref}>Parent element</div>
            <Child ref={ref} />
        </div>
    );
};
```

# Element lifecycle

This is not the component lifecycle. In fact, there is no component lifecycle. That's what hooks are for!

These lifecycle callbacks are element-specific. The diffing algorithm calls these callbacks on an element if they exist.

- onRemove: called when the element is removed during the render process.
- onUpdate: called when the element has been updated during the render process, even when nothing about the element changes.
- onCreate: called when the element is first created during the render process.

For example:

```javascript
const MyComponent = () => {
    return (
        <Fragment>
            <div
                onCreate={() => console.log("I was created.")}
                onRemove={() => console.log("I was removed.")}
            >
                ...
            </div>
        </Fragment>
    );
};
```

# Routing

There are a few different ways to render a route:

- Pass children to a `Route` component.
- Pass a component to the `component` prop of a `Route` component.
- Use the `Route` component's `render` prop. The `render` prop will be called with a route `match` object that looks like: `{ params: {}, query: {} }`.

## Route component

| Prop | Description |
|------|-------------|
|path| The path that the current URL must match in order for the Route's component prop, or children, to be rendered.|
|sensitive|When true the regexp will be case sensitive. (default: false)|
|strict|When true the regexp won't allow an optional trailing delimiter to match. (default: false)|
|end|When true the regexp will match to the end of the string. (default: true)|

```javascript
import { Link, Route, routeParams } from "mir";

const Home = () => <div>Home</div>;
const User = ({ match }) => <div>Viewing user {match.params.id}</div>;

const Component = () => {
    return (
        <div>
            <div>
                <Link to="/user/123">View user 123</Link>
                <Link to="/">Go home</Link>
            </div>
            <div>
                <Route path="/"><Home/></Route>
                <Route path="/user/:id" component={User} />
                <Route path="/user/:id">
                    <div>
                        This is also a match for `/user/:id`. To access params,
                        since this is not a component that can receive props, use
                        the `routeParams` import: {routeParams.id}
                    </div>
                </Route>
                <Route path="/post/:id" render={({ match }) => (<div>Post {match.params.id}</div>)} />
            </div>
        </div>
    );
};
```

## Link component

The `Link` component can be used to trigger a route change.
`Link` simply renders an anchor with a click handler that calls `goto(to)` where `to` is `Link`'s `to` prop.
You can either create your own implementation or style the provided `Link`. Both of these approaches are fairly straightforward.

## goto

To programatically change routes, use the `goto` export. It will trigger event handlers that need to be
called in order the routes to be matched and updated.

```javascript
import { goto } from "mir";

goto("/somewhere");
```

## preRenderRouteHandlers

```javascript
import { registerPreRenderRouterHandler } from "mir";

registerPreRenderRouterHandler(() => {
    // ...
});
```

## postRenderRouteHandlers

These handlers are fired after the render pass. This is useful if, say, you need to
call a syntax highlighter on new additions to the DOM after a route change.

```javascript
import { registerPostRenderRouterHandler } from "mir";

registerPostRenderRouterHandler(() => {
    // ...
});
```

# Hooks

Included hooks:

- [x] useEffect
- [x] useLayoutEffect
- [x] useRef
- [x] useState
- [x] useCallback
- [x] useMemo
- [x] useReducer
- [x] useContext

## useEffect

- Same API and behavior as React useEffect.
- Not dissimilar to React useEffect, but still worth mentioning: effects are guaranteed to run after the DOM renders.

## useRef

- Same API and behavior as React useRef.
- Not dissimilar to React useRef, but still worth mentioning: element refs are assigned during the render process, so they are safe to reference in an effect callback.

## useState

- Same API and behavior as React useState.

## useCallback

- Same API and behavior as React useCallback.

## useMemo

- Same API and behavior as React useMemo.

## useReducer

- Same API and behavior as React useReducer.

## useContext
### Defining

```javascript
import { createContext, useContext } from "mir";

const themes = {
    light: {
        foreground: "#000000",
        background: "#eeeeee"
    },
    dark: {
        foreground: "#ffffff",
        background: "#222222"
    }
};

const ThemeCtx = createContext(themes.light);
// or
// const ThemeCtx = createContext();
// ThemeCtx.provide(themes.light);

// You now have..
// <ThemeCtx.Provider />
// <ThemeCtx.Consumer />
// ThemeCtx.provide()

// provide() will queue a render. This is useful if context needs
// to be mutated and components that depend on this state should be re-evaluated.
```

### useContext

The simplest, most idiomatic way to consume a context:

```javascript
const Component = () => {
    const theme = useContext(ThemeCtx);
    return <button style={{background:theme.background}}>Button</button>;
};
```

### Consumer

*Consumers don't care about their upstream Provider's context values.*
They instead reach directly into the context they are tied to, which
ends up being themes.light in this case.

The direct child of a consumer should be a function that accepts the value
of whatever is stored in context.

```javascript
const Component = () => {
    return (
        <ThemeCtx.Consumer>
            {theme => (<button style={{background:theme.background}}>Button</button>)}
        </ThemeCtx.Consumer>
    );
};
```

### Provider

Providing new context to the `value` prop of a Provider will mutate the context for
all receivers of this context, not just downstream children. Mutating context in this
way does not trigger a re-render.

If you want to mutate context while also triggering a re-render, use the `provide` method.

Providers do not need to be ancestors of a `Consumer` in order to provide context. In fact,
you don't even need to use a `Provider` anywhere to supply context to a component.
Instead, you can rely solely on `useContext` and `Consumer` anywhere in your application.

```javascript
const Parent = () => {
    return (
        <ThemeCtx.Provider value={themes.dark}>
            <... />
        </ThemeCtx.Provider>
    );
};
```

# defineStore and useStore

This state management implementation is useful when you have distant components that should share the same state.
Maybe they don't exist within the same render target. Maybe they do. It doesn't matter.

## defineStore

`defineStore` takes a storage key, an initial state, and some options:

|Option|Meaning|
|------|------------|
|persist|If true, then state will be persisted to localStorage using the provided storage key. (default: true)|
|forceUpdates|If true, then dispatched actions will trigger a re-render. (default: true)|

First, define a store with whatever actions it may need:

```javascript
import { defineStore } from "mir";

const countStore = defineStore("countStore", { count: 0 }, { persist: false, forceUpdates: true });

countStore.actions = {
    up: (state) => ({ count: state.count + 1 }),
    upBy: (state, c) => ({ count: state.count + c }),
};
```

Elsewhere, consume that store with `useStore`.

```javascript
import { useStore } from "mir";

const CountComponent = () => {
    const [countState, countDispatch] = useStore(countStore);

    return (
        <button onClick={() => countDispatch("up")}>+1</button>
        <button onClick={() => countDispatch("upBy", 5)}>+5</button>
    );
};
```

# Component-level attributes

In some cases, it can be handy to define attributes directly on the 
component function for later use.

```javascript
const Input = () => <input ... />;
const InputPrefix = () => <span />;
const InputSuffix = () => <span />;

Input.id = "Input";
InputPrefix.id = "InputPrefix";
InputSuffix.id = "InputSuffix";
```

At runtime, these attributes can be accessed at child.Component.*attribute*.

```javascript
const Group = ({ children }) => {
    const validChildren = children.filter(c => c?.Component?.id !== undefined);
    const styles = {};

    validChildren.forEach(c => {
        if (c.Component.id === "InputPrefix") styles.pl = "30px";
        if (c.Component.id === "InputSuffix") styles.pr = "30px";
    });

    const clones = validChildren.map(c => {
        return c.Component.id === "Input"
            ? cloneElement(c, styles)
            : c;
    });

    return <div>{clones}</div>;
};

Input.Group = Group;
Input.Prefix = InputPrefix;
Input.Suffix = InputSuffix;
```

...which supports these kinds of patterns:

```javascript
const Component = () => {
    return (
        <Input.Group>
            <Input.Prefix>$</Input.Prefix>
            <Input>100.00</Input>
        </Input.Group>
    )
};
```
