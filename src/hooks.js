import { enqueueRender } from "./render";
import { depsChanged, EMPTY_ARRAY, invokeOrReturn, undef } from "./utils";

export let wipComponent, hookIndex;
export let postEffects = [];
export const clearEffects = () => (postEffects = []);
export let pendingCleanups = {};
export let pendingLayoutCleanups = {};
export let componentsWithState = {};
export let hookIndexToCleanup = {};
export let checkIns = {};
export let clearCheckins = () => (checkIns = {});
export let allComponents = {};

export const resetHookState = (node) => {
	hookIndex = 0;
	wipComponent = node;
	allComponents[wipComponent] = wipComponent;
};

export const removeAllHookStates = () => {
	for (const component of Object.keys(allComponents)) {
		if (component.hooks) delete component.hooks;
	}
};

export const removeHookStateForComponent = (component, uniqueNumber) => {
	hookRefs[component][uniqueNumber].hook._store = [];
	componentsWithState[component] = componentsWithState[component].slice(componentsWithState[component].indexOf(uniqueNumber), 1);
};

let hookRefs = {};
const storeHooksRef = (component, uniqueNumber, hook) => {
	// https://stackoverflow.com/questions/2631001/test-for-existence-of-nested-javascript-object-key
	((hookRefs[component] = hookRefs[component] || {})[uniqueNumber] = hookRefs[component][uniqueNumber] || {}).hook = hook;
};

const createHookState = () => {
	return { _store: [], _effects: [], _cleanups: [], _refs: [] };
};

export const getHookState = (index) => {
	if (!wipComponent) return;
	if (!wipComponent.hooks) wipComponent.hooks = {};

	const hook = wipComponent.hooks[uniqueMap[wipComponent]] || (wipComponent.hooks[uniqueMap[wipComponent]] = createHookState());
	if (hook._store.length <= index) hook._store.push({});

	// Only store hookRef if not already stored -- will this save on performance?
	!hookRefs?.[wipComponent]?.[uniqueMap[wipComponent]]?.hook && storeHooksRef(wipComponent, uniqueMap[wipComponent], hook);

	!(componentsWithState[wipComponent] = componentsWithState[wipComponent] || []).includes(uniqueMap[wipComponent]) &&
		componentsWithState[wipComponent].push(uniqueMap[wipComponent]);

	return hook._store[index];
};

export const useReducer = (reducer, initialArg, init) => {
	const hook = getHookState(hookIndex++);
	if (!hook._component) {
		hook._component = wipComponent;
		hook._reducer = reducer;
		hook._value = [
			!init ? invokeOrReturn(undef, initialArg) : init(initialArg),
			(action) => {
				const next = hook._reducer(hook._value[0], action);
				if (hook._value[0] !== next) {
					hook._value[0] = next;
					enqueueRender();
				}
			},
		];
	}

	return hook._value;
};

export const useMemo = (factory, deps) => {
	const hook = getHookState(hookIndex++);
	if (depsChanged(hook._deps, deps)) {
		hook._deps = deps;
		hook._factory = factory;
		hook._value = factory();
	}
	return hook._value;
};

export const useState = (initialState) => useReducer(invokeOrReturn, initialState);

export const useRef = (initialVal) => {
	let factory = () => ({ current: initialVal });
	return useMemo(factory, EMPTY_ARRAY);
};

export const useImperativeHandle = (ref, createHandle, deps) => {
	const hook = getHookState(hookIndex++);
	if (depsChanged(hook._deps, deps)) {
		hook._deps = deps;
		ref.current = createHandle();
	}
};

export const useCallback = (callback, deps) => useMemo(() => callback, deps);

export let uniqueMap = {};
export const clearUniqueMap = () => (uniqueMap = {});

export const incrementUniqueMap = (component) => {
	let current = uniqueMap[component] || 0;

	// If a component doesn't have a key property, then it...
	// ...wasn't explicitly assigned a key prop. h assigns component.key...
	// ...from props.key.
	// TODO: Optimize. This is terrible.
	if (component.key) {
		current = `${current}`;
		if (current.includes("___")) {
			let index = current.match(/^(\d+)/)[0];
			let key = current.replace(/^\d+___/, "");

			if (key !== component.key) {
				current = `${index}___${component.key}`;
			} else {
				// If the key is the same, increment the unique index
				index = parseInt(index) + 1;
			}
		} else {
			current = `${current}___${component.key}`;
		}
	} else {
		current = current + 1;
	}

	checkIns[component] = checkIns[component] || [];
	checkIns[component].push(current);
	uniqueMap[component] = current;
};

export const useEffect = (callback, deps) => {
	const component = wipComponent;
	const index = hookIndex++;
	const hook = getHookState(index);
	const uniqueComponentIndex = uniqueMap[component];

	postEffects.push([
		(uniqueNumber) => {
			if (depsChanged(hook._deps, deps) || hook.remounted) {
				if (!hook.remounted) hook._cleanups?.forEach((c) => c());
				if (hook.remounted) hook.remounted = undefined;
				hook._cleanups = [];
				hook._deps = deps;
				hook._factory = callback;
				hook._value = callback();
				if (checkIns[component].includes(uniqueComponentIndex)) {
					if (typeof hook._value === "function") {
						hook._cleanups.push(hook._value);
						(((hookIndexToCleanup[component] = hookIndexToCleanup[component] || {})[uniqueNumber] = hookIndexToCleanup[component][uniqueNumber] || {})[index] =
							hookIndexToCleanup[component][uniqueNumber][index] || {}).func = hook._value;
						hookIndexToCleanup[component][uniqueNumber][index].hook = hook;
						((pendingCleanups[component] = pendingCleanups[component] || {})[uniqueNumber] = pendingCleanups[component][uniqueNumber] || []).push(index);
					}
				}
			}
		},
		uniqueComponentIndex,
	]);
};

export const useLayoutEffect = (callback, deps) => {
	const component = wipComponent;
	const index = hookIndex++;
	const hook = getHookState(index);
	const uniqueComponentIndex = uniqueMap[component];

	if (depsChanged(hook._deps, deps) || hook.remounted) {
		if (!hook.remounted) hook._cleanups?.forEach((c) => c());
		if (hook.remounted) hook.remounted = undefined;
		hook._cleanups = [];
		hook._deps = deps;
		hook._factory = callback;
		hook._value = callback();
		if (typeof hook._value === "function") {
			hook._cleanups.push(hook._value);
			(((hookIndexToCleanup[component] = hookIndexToCleanup[component] || {})[uniqueComponentIndex] = hookIndexToCleanup[component][uniqueComponentIndex] || {})[index] =
				hookIndexToCleanup[component][uniqueComponentIndex][index] || {}).func = hook._value;
			hookIndexToCleanup[component][uniqueComponentIndex][index].hook = hook;
			((pendingLayoutCleanups[component] = pendingLayoutCleanups[component] || {})[uniqueComponentIndex] = pendingLayoutCleanups[component][uniqueComponentIndex] || []).push(
				index
			);
		}
	}
};

export const createContext = (value) => {
	const ctx = { value, provide };
	ctx.Provider = Provider.bind(ctx);
	ctx.Consumer = Consumer.bind(ctx);
	return ctx;
};

export const useContext = (ctx) => ctx.value;

function provide(value) {
	if (this.value !== value) {
		this.value = value;
	}

	enqueueRender();
}

function Provider() {
	if (arguments?.[0]?.value && this.value !== arguments?.[0]?.value) {
		this.value = arguments?.[0].value;
	}
	return arguments[0].children;
}

function Consumer() {
	if (typeof arguments[0].children?.[0]?.name === "function") {
		return arguments[0].children[0].name(this.value);
	}
	console.warn("Expected type function as direct descendant of Consumer. Got:", arguments[0].children);
	return arguments[0].children;
}
