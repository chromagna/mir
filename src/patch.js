import { EMPTY_ARRAY, EMPTY_OBJECT, eventProxy, map, RECYCLED, SVG_NS, TEXT, XLINK_NS } from "./utils";

const updateProperty = (element, name, lastValue, nextValue, isSvg) => {
	name = name == "class" && !isSvg ? "className" : name;

	if (name === "key") {
	} else if (name === "style") {
		for (const i in { ...lastValue, ...nextValue }) {
			const style = nextValue == null || nextValue[i] == null ? "" : nextValue[i];
			if (i[0] === "-") {
				element[name].setProperty(i, style);
			} else {
				element[name][i] = style;
			}
		}
	} else if (name === "ref") {
		try {
			nextValue.current = element;
		} catch (e) {}
		element.removeAttribute(name);
	} else {
		if (/^on\w+/.test(name)) {
			if (!element.events) element.events = {};
			element.events[(name = name.slice(2).toLowerCase())] = nextValue;
			if (nextValue == null) {
				element.removeEventListener(name.toLowerCase(), eventProxy);
			} else if (lastValue == null) {
				element.addEventListener(name.toLowerCase(), eventProxy);
			}
		} else {
			const nullOrFalse = nextValue == null || nextValue === false;
			if (name in element && name !== "list" && name !== "draggable" && name !== "spellcheck" && name !== "translate" && !isSvg && element.nodeName !== "svg") {
				element[name] = nextValue == null ? "" : nextValue;
				if (nullOrFalse) element.removeAttribute(name);
			} else {
				const ns = isSvg && name !== (name = name.replace(/^xlink:?/, ""));
				if (ns) {
					if (nullOrFalse) {
						element.removeAttributeNS(XLINK_NS, name);
					} else {
						element.setAttributeNS(XLINK_NS, name, nextValue);
					}
				} else {
					if (nullOrFalse) {
						element.removeAttribute(name);
					} else {
						element.setAttribute(name, nextValue);
					}
				}
			}
		}
	}
};

const requiresNS = (isSvg, node) => (isSvg = isSvg || node.name === "svg" || node.name === "path");

const stripDefaultProps = (props) => {
	if (props.__hasDefaultProps) {
		Object.values(props.__hasDefaultProps).map((key) => {
			delete props[key];
		});
		delete props.__hasDefaultProps;
	}

	return props;
};

export const createElement = (node, lifecycle, isSvg) => {
	const element =
		node.type == TEXT ? document.createTextNode(node.name) : requiresNS(isSvg, node) ? document.createElementNS(SVG_NS, node.name) : document.createElement(node.name);

	let props = node.props;
	props = stripDefaultProps(props);

	if (props.onCreate) lifecycle.push(() => props.onCreate(element));

	for (let i = 0, length = node.children.length; i < length; i++) {
		element.appendChild(createElement(node.children[i], lifecycle, isSvg));
	}

	for (const name in props) updateProperty(element, name, null, props[name], isSvg);

	return (node.element = element);
};

const updateElement = (element, lastProps, nextProps, lifecycle, isSvg, isRecycled) => {
	nextProps = stripDefaultProps(nextProps);

	for (const name in { ...lastProps, ...nextProps }) {
		if ((name === "value" || name === "checked" ? element[name] : lastProps[name]) !== nextProps[name]) {
			updateProperty(element, name, lastProps[name], nextProps[name], isSvg);
		}
	}

	const cb = isRecycled ? nextProps.onCreate : nextProps.onUpdate;
	if (cb != null) lifecycle.push(() => cb(element, lastProps));
};

const removeChildren = (node) => {
	for (let i = 0, length = node.children.length; i < length; i++) {
		removeChildren(node.children[i]);
	}

	const cb = node.props.onRemove;
	if (cb != null) cb(node.element);

	return node.element;
};

const removeElement = (parent, node) => {
	const remove = () => parent.removeChild(removeChildren(node));
	const cb = node.props && node.props.onRemove;
	if (typeof cb === "function") cb(node.element, remove);
	remove();
};

const getKey = (node) => (node == null ? null : node.key);

const createKeyMap = (children, start, end) => {
	const out = {};
	let key, node;

	for (; start <= end; start++) {
		if ((key = (node = children[start]).key) != null) {
			out[key] = node;
		}
	}

	return out;
};

const patchElement = (parent, element, lastNode, nextNode, lifecycle, isSvg) => {
	if (nextNode === lastNode) {
	} else if (lastNode != null && lastNode.type === TEXT && nextNode.type === TEXT) {
		if (lastNode.name !== nextNode.name) element.nodeValue = nextNode.name;
	} else if (lastNode == null || lastNode.name !== nextNode.name) {
		const newElement = parent.insertBefore(createElement(nextNode, lifecycle, isSvg), element);

		if (lastNode != null) removeElement(parent, lastNode);

		element = newElement;
	} else {
		updateElement(element, lastNode.props, nextNode.props, lifecycle, (isSvg = isSvg || nextNode.name === "svg"), lastNode.type === RECYCLED);

		let savedNode, childNode, lastKey, nextKey;

		const lastChildren = lastNode.children;
		const nextChildren = nextNode.children;

		let lastChStart = 0;
		let nextChStart = 0;

		let lastChEnd = lastChildren.length - 1;
		let nextChEnd = nextChildren.length - 1;

		while (nextChStart <= nextChEnd && lastChStart <= lastChEnd) {
			lastKey = getKey(lastChildren[lastChStart]);
			nextKey = getKey(nextChildren[nextChStart]);

			if (lastKey == null || lastKey !== nextKey) {
				break;
			}

			patchElement(element, lastChildren[lastChStart].element, lastChildren[lastChStart], nextChildren[nextChStart], lifecycle, isSvg);

			lastChStart++;
			nextChStart++;
		}

		while (nextChStart <= nextChEnd && lastChStart <= lastChEnd) {
			lastKey = getKey(lastChildren[lastChEnd]);
			nextKey = getKey(nextChildren[nextChEnd]);

			if (lastKey == null || lastKey !== nextKey) {
				break;
			}

			patchElement(element, lastChildren[lastChEnd].element, lastChildren[lastChEnd], nextChildren[nextChEnd], lifecycle, isSvg);

			lastChEnd--;
			nextChEnd--;
		}

		if (lastChStart > lastChEnd) {
			while (nextChStart <= nextChEnd) {
				element.insertBefore(createElement(nextChildren[nextChStart++], lifecycle, isSvg), (childNode = lastChildren[lastChStart]) && childNode.element);
			}
		} else if (nextChStart > nextChEnd) {
			while (lastChStart <= lastChEnd) {
				removeElement(element, lastChildren[lastChStart++]);
			}
		} else {
			const lastKeyed = createKeyMap(lastChildren, lastChStart, lastChEnd);
			const nextKeyed = {};

			while (nextChStart <= nextChEnd) {
				lastKey = getKey((childNode = lastChildren[lastChStart]));
				nextKey = getKey(nextChildren[nextChStart]);

				if (nextKeyed[lastKey] || (nextKey != null && nextKey === getKey(lastChildren[lastChStart + 1]))) {
					if (lastKey == null) removeElement(element, childNode);
					lastChStart++;
					continue;
				}

				if (nextKey == null || lastNode.type === RECYCLED) {
					if (lastKey == null) {
						patchElement(element, childNode && childNode.element, childNode, nextChildren[nextChStart], lifecycle, isSvg);
						nextChStart++;
					}
					lastChStart++;
				} else {
					if (lastKey === nextKey) {
						patchElement(element, childNode.element, childNode, nextChildren[nextChStart], lifecycle, isSvg);
						nextKeyed[nextKey] = true;
						lastChStart++;
					} else {
						if ((savedNode = lastKeyed[nextKey]) != null) {
							patchElement(element, element.insertBefore(savedNode.element, childNode && childNode.element), savedNode, nextChildren[nextChStart], lifecycle, isSvg);
							nextKeyed[nextKey] = true;
						} else {
							patchElement(element, childNode && childNode.element, null, nextChildren[nextChStart], lifecycle, isSvg);
						}
					}
					nextChStart++;
				}
			}

			while (lastChStart <= lastChEnd) {
				if (getKey((childNode = lastChildren[lastChStart++])) == null) {
					removeElement(element, childNode);
				}
			}

			for (const key in lastKeyed) {
				if (nextKeyed[key] == null) {
					removeElement(element, lastKeyed[key]);
				}
			}
		}
	}

	return (nextNode.element = element);
};

export const createVNode = (name, props, children, element, key, type) => {
	return { children, element, key, name, props, type };
};

export const createTextVNode = (text, element) => createVNode(text, EMPTY_OBJECT, EMPTY_ARRAY, element, null, TEXT);

const recycleChild = (element) => (element.nodeType === 3 ? createTextVNode(element.nodeValue, element) : recycleElement(element));

const recycleElement = (element) => {
	return createVNode(element.nodeName.toLowerCase(), EMPTY_OBJECT, map.call(element.childNodes, recycleChild), element, null, RECYCLED);
};

export const patch = (lastNode, nextNode, container) => {
	let lifecycle = [];

	patchElement(container, container.children[0], lastNode, nextNode, lifecycle);

	while (lifecycle.length > 0) lifecycle.pop()();

	return nextNode;
};
