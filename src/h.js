import { incrementUniqueMap, resetHookState } from "./hooks";
import { createTextVNode, createVNode } from "./patch";
import { DEFAULT, EMPTY_OBJECT, isArray } from "./utils";

/**
 * e.g.:
 * const clone = cloneElement(props.children[0], { color: "blue" });
 */
export const cloneElement = (el, props = {}) => {
	if (el.Component) {
		const c = el.Component(Object.assign(el.props, props));
		if (el.Component.key) c.key = `${el.Component.key}_C`;
		return c;
	} else {
		const e = {};
		e.props = Object.assign({}, el.props);
		e.props.children = Object.assign([], el.children);
		return h(el.name, e.props);
	}
};

export const h = function (name, props) {
	let node,
		length = arguments.length;
	let rest = [];
	let children = [];

	for (; length-- > 2; rest.push(arguments[length]));

	if ((props = props == null ? {} : props).children != null) {
		rest.length <= 0 && rest.push(props.children);

		delete props.children;
	}

	while (rest.length > 0) {
		if (isArray((node = rest.pop()))) {
			for (length = node.length; length-- > 0; rest.push(node[length]));
		} else if (node === false || node === true || node == null) {
		} else {
			children.push(typeof node === "object" ? node : createTextVNode(node));
		}
	}

	if (typeof name === "function") {
		props.children = props.children || children;

		if (props.key) name.key = props.key;
		if (name.defaultProps) props = Object.assign(props, name.defaultProps, { __hasDefaultProps: Object.keys(name.defaultProps) });

		incrementUniqueMap(name);
		resetHookState(name);

		const el = name(props);
		if (el) el.Component = name;
		return el;
	} else {
		return createVNode(name, props, children, null, props.key, DEFAULT, EMPTY_OBJECT);
	}
};
